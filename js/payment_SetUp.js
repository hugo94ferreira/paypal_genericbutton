// Set up a payment
payment: function(data, actions) {
  return actions.payment.create({
    transactions: [{
      amount: {
        total: '',
        currency: '',
        details: {
          subtotal: '',
          tax: '',
          shipping: '',
          handling_fee: '',
          shipping_discount: '',
          insurance: ''
        }
      },
      description: '',
      custom: '',
      invoice_number: ''
      payment_options: {
        allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
      },
      soft_descriptor: '',
      item_list: {
        items: [
        {
          name: '',
          description: '',
          quantity: '',
          price: '',
          tax: '',
          sku: '',
          currency: ''
        }],
        shipping_address: {
          recipient_name: '',
          line1: '',
          line2: '',
          city: '',
          country_code: '',
          postal_code: '',
          phone: '',
          state: ''
        }
      }
    }],
    note_to_payer: ''
  });
}